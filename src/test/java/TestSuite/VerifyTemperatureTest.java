package TestSuite;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.Test;

public class VerifyTemperatureTest {

	@Test
	public void validateTemperature()
	{
		
		System.setProperty("webdriver.chrome.driver", "C:\\chromedriver.exe");
		WebDriver driver =new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("https://www.theweathernetwork.com/ca");
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		
		String str=driver.findElement(By.xpath("//div[@class='current-location-current-temp-c']")).getText().toString();
		int res=Integer.parseInt(str.substring(0, 2));
		System.out.println(res);
		
		if(res<15)
		{
			System.out.println("Test has failed");
		}
		
		else
		{
			System.out.println("Test has passed");
		}
		driver.close();
		

		
		
			

	}

}
