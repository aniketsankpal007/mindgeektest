package TestSuite;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.Test;

public class ButtonClick_SeeAllNewsTest {
	
	@Test
	public void clickbuttonSeeAllNews() throws InterruptedException
	{
		// TODO Auto-generated method stub
		System.setProperty("webdriver.chrome.driver", "C:\\chromedriver.exe");
		WebDriver driver =new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("https://www.theweathernetwork.com/ca");
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);		
		Thread.sleep(5000);
		driver.findElement(By.linkText("See All News")).click();
		driver.close();

	}

}
